package app;

public class Arbol<T extends Comparable<T>>   {
	private Nodo <T> raiz;
	public Arbol(T v){
		raiz = new Nodo <T>(v);
	}
	public Arbol(){
		raiz = new Nodo <T>();
	}
	public void  add (T v){
		add(v,raiz);
	}
	public void  add (T v , Nodo<T> n ){
		if(raiz.getValue()== null) 
				raiz.setValue(v);
		else{
		if (v.compareTo((T) n.getValue()) >= 0)
			if(n.TieneMayor())
				add(v,n.getMay());
			else
				n.setMay(v);
		
		else
			if(n.TieneMenor())
				add(v,n.getMen());
			else
				n.setMen(v);
		}
		}
	
	void printInOrder(){
		if(!isEmpty())
		printInOrder(raiz);
		System.out.println();
	}
	void printInOrder(Nodo<T> n){
		
		if (n.TieneMenor())
			printInOrder(n.getMen());
		
		System.out.print(n.getValue().toString() +" ");
	   
	    if (n.TieneMayor())
			printInOrder(n.getMay());
				
	}
	public void printPreOrder(){
		printPreOrder(raiz);
		System.out.println();
	}
	
	public void printPreOrder(Nodo n){
		
		System.out.print(n.val +" ");
		
		if (n.TieneMenor())
			printInOrder(n.getMen());
		
	    if (n.TieneMayor())
			printInOrder(n.getMay());
				
		
	}
	public void printPostOrder(){
		printPostOrder(raiz);
		System.out.println();
	}
	public void printPostOrder(Nodo n){

		if (n.TieneMenor())
			printInOrder(n.getMen());
		   
	    if (n.TieneMayor())
			printInOrder(n.getMay());
	    
	    System.out.print(n.val +" ");
				
	}
	public Nodo<T> isChild(T n){		
		return isChild(n,raiz,null);

	}
	public Nodo<T> isChild(T v,Nodo nod,Nodo paps){
		if(v.compareTo((T)nod.getValue())== 0)return paps;
		if(v.compareTo((T)nod.getValue())>= 0 && nod.getMay()!=null)return isChild(v,nod.getMay(),nod);
		if(v.compareTo((T)nod.getValue())<= -1 && nod.getMen()!=null)return isChild(v,nod.getMen(),nod);
		return null;
	}
	
	public Nodo<T> isChild(Nodo n){		
		return isChild(n,raiz,null);
	}
	public Nodo<T> isChild(Nodo v,Nodo nod,Nodo paps){
		if(v.getValue().compareTo((T)nod.getValue())== 0){
			if(v.getMay()== nod.getMay()&& v.getMen()== nod.getMen())
			return paps;
		}
		if(v.getValue().compareTo((T)nod.getValue())>= 0 && nod.getMay()!=null)return isChild(v,nod.getMay(),nod);
		if(v.getValue().compareTo((T)nod.getValue())<= -1 && nod.getMen()!=null)return isChild(v,nod.getMen(),nod);
		return null;
	}
	
	public Nodo<T> searchProf(T v){
		return searchProf(v,raiz);
	}
	public Nodo<T> searchProf(T v,Nodo nod){
		if(v.compareTo((T)nod.getValue())== 0) return nod;
		if(v.compareTo((T)nod.getValue())>=  1 && nod.getMay()!=null)return searchProf(v,nod.getMay());
		if(v.compareTo((T)nod.getValue())<= -1 && nod.getMen()!=null)return searchProf(v,nod.getMen());
		return null;
	}

	public Nodo<T> findMin(Nodo n){
		Nodo<T> temp = n;
		while(temp.TieneMenor())
			temp = temp.getMen();
		
		return temp;
	}
	
	public Nodo<T> findMax(Nodo n){
		Nodo<T> temp = n;
		while(temp.TieneMayor())
			temp = temp.getMay();
		return temp;
	}
	
	public boolean isEmpty(){
	if(!raiz.equals(null)){
		if( raiz.getValue() != null)
			return false;
		return true;
	}
	return true;
	}
	
	public int maxDepth(){
		if(!isEmpty())
		return maxDepth(raiz);
		else
			return 0;
	}
	private int maxDepth(Nodo n){
		int n1=0,n2=0;
		if(n.TieneMayor()){
			  n1 = maxDepth(n.getMay())+1;
		}
		if(n.TieneMenor()){
			n2 = maxDepth(n.getMen())+1;
		}
		if(!n.TieneMayor()&&!n.TieneMenor())
		return 1;
		
		if(n1>n2) return n1;
		else return n2;
	}
	public void Clear(){
		raiz.setMen((Nodo)null);
		raiz.setMay((Nodo)null);
		raiz.setValue(null);
	}

	public boolean remove(T v){
		return remove(v,raiz);
	}
	public boolean remove(T v,Nodo temp){
	   temp = searchProf(v,temp);
		if(temp!= null){
			if(temp.TieneMayor()){
				   Nodo <T> m = findMin(temp.getMay());
				    T numT= m.getValue();
				    remove(numT,m);	
					temp.setValue(numT);				
			}
			else if(temp.TieneMenor()){
				Nodo <T> m = findMax(temp.getMen());
			    T numT= m.getValue();
				 remove(numT,m);
			      temp.setValue(numT);				
			}
			else{
				Nodo<T>papa = isChild(temp);
				if(papa!=null){
					if(papa.getMay()!= null && papa.getMay().getValue().equals(v))
						papa.setMay((Nodo)null);
					else if(papa.getMen()!= null && papa.getMen().getValue().equals(v))
						papa.setMen((Nodo)null);
					
					
				}
				else
					raiz.setValue(null);
			}
				return true;				
		}
		System.out.println("no existe");
		return false;
	}
	public void setRaiz(T v){
		raiz.setValue( v);
	}
	public Nodo<T> getRaiz(){
		return raiz;
	}
	public boolean exists( T v){
		
		return (searchProf(v)!=null);
	}
	
    public void recAmplitud(){
        recAmplitud(raiz);
    }

    private void recAmplitud(Nodo nodo){
        int max = 0;
        int nivel = maxDepth();

        for ( ; nivel >= 0; nivel--)
            max += Math.pow(2, nivel);
        max++;     

        Nodo queue[] = new Nodo[ max ];
        queue[1] = nodo;
        int x = 1;
        for (int i = 2; i < max; i += 2, x++){
            if (queue[x] == null){
                queue[i] = null;
                queue[i + 1] = null;
            }
            else{
                queue[i]   = queue[x].getMen(); 
                queue[i + 1] = queue[x].getMay();
            }
        }
        nivel = 0;
        int cont = 0;                       
        int cantidad = 1;                   
        int ultimaPosicion = 1;            
        for (int i = 1; i < max; i++){
            if(i == Math.pow(2, nivel)){
            	System.out.print("\n Nivel " + (nivel) + ": ");
                nivel++;
            }
            if( queue[i] != null ){
                System.out.print("[" + queue[i].getValue() + "]");
                cont++;
            }
            if(ultimaPosicion == i  && cantidad == Math.pow(2, --nivel)){
                if(cantidad == 1)
                    System.out.print(" Cantidad de nodos: " + cont + " (raiz)");
                else
                    System.out.print(" Cantidad de nodos: " +  cont);
                cont = 0;
                cantidad *= 2;
                ultimaPosicion += (int)Math.pow(2, ++nivel);
            }
        }
    }
	
	
}
