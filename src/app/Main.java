
package app;

public class Main {
	
	
 public static void main(String[] args) {
	Arbol <Alumno> arbolAl = new Arbol<Alumno>();
	Arbol <Integer> num = new Arbol<Integer>();
	
	Alumno[] arr = {new Alumno("sd",10d)
			,new Alumno("",9d)
			,new Alumno("",5d)
			,new Alumno("",0d)
			};
	arbolAl.setRaiz(arr[0]);
	for (int i = 1; i < arr.length; i++) {
		arbolAl.add(arr[i]);
	}
	System.out.println("Print InOrder calificaciones");
	arbolAl.printInOrder();
	System.out.println("Print PreOrder calificaciones");
	arbolAl.printPreOrder();
	System.out.println("Print PostOrder calificaciones");
	arbolAl.printPostOrder();
	
	int n = 100;
	for (int i = 0; i < n; i++) {
		num.add((int)(Math.random()*n)+1);
	}
	System.out.println("Print InOrder numeros");
	num.printInOrder();
	System.out.println("Profundidad: " + num.maxDepth());
	System.out.println("Recorrido amplitud: ");
	num.recAmplitud();
	System.out.println("\n Se limpia y se imprime: ");
	num.Clear();
	num.printInOrder();

}
 
}
