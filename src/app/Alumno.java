package app;

import java.util.Comparator;

public  class Alumno implements Comparable<Alumno>,Comparator<Alumno>{
String nombre =null;
double promedio = 0.0d;
public Alumno (){
	nombre =null;
	promedio = 0.0d;
}
public Alumno (String n, double d){
	nombre = n;
	promedio = d;
}
public String getNombre() {
	return nombre;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
public double getPromedio() {
	return promedio;
}
public void setPromedio(double promedio) {
	this.promedio = promedio;
}
public int compareTo(Alumno a) {
	if(a.getPromedio()> this.promedio) return -1;
	if(a.getPromedio()< this.promedio) return 1;
	return 0;
}
@Override
public int compare(Alumno o1, Alumno o2) {
	if(o1.getPromedio()> o2.getPromedio()) return -1;
	if(o1.getPromedio()< o2.getPromedio()) return 1;
	return 0;
}
@Override
public String toString() {
	return promedio +""; 
}

	
}
